<x-app-layout>
    <div class="py-3">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-700">
                    <thead class="bg-gray-200">
                        <tr>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Title') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Project') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Status') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Actions') }}
                            </th>              
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach ($tasks as $task)
                            <tr>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    {{ $task->title }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    {{ $task->project->title }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    <div class="flex items-center justify-between">
                                        @if(!$task->completed)
                                            <div style="padding-top: 0.1em; padding-bottom: 0.1rem;" class="text-xs px-3 bg-orange-200 text-orange-800 rounded-full">Pending</div>
                                        @else
                                            <div style="padding-top: 0.1em; padding-bottom: 0.1rem" class="text-xs px-3 bg-teal-200 text-teal-800 rounded-full">Completed</div>
                                        @endif
                                    </div>
                                </td>
                                @can('manage-tasks', $task)
                                    <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                        <div class="flex gap-2 justify-items-start">
                                            <button onclick='Livewire.emit("openModal", "edit-task", {{ json_encode([$task]) }})'
                                                class="px-4 py-2 font-medium tracking-wide capitalize transition-colors duration-200 transform rounded-md focus:outline-none focus:ring focus:ring-blue-300 focus:ring-opacity-80bg-yellow-400 bg-yellow-400 text-gray-600 hover:text-black hover:bg-yellow-500"
                                                >{{ __('Edit') }} 
                                            </button>

                                            <form method="POST" action="{{ route('tasks.destroy', $task) }}">
                                                @csrf
                                                @method('DELETE')
                                                <x-jet-secondary-button type="submit" onclick="return confirm('Are you sure?')" class="bg-red-500 text-gray-800 hover:text-black hover:bg-red-600 inline-block">
                                                    {{ __('Delete') }}
                                                </x-jet-secondary-button>
                                            </form>
                                        </div>
                                    </td>
                                @endcan
                            </tr>  
                        @endforeach 
                    </tbody>
                </table>
            </div>
            <div class="mt-2">
                {{ $tasks->links() }}
            </div>
        </div>
    </div>
</x-app-layout>