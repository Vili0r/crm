<x-app-layout>
    <div class="py-3">

        @role('admin')
        <div class="mb-3 ml-8">
            <x-jet-secondary-button class="bg-green-700 text-gray-300 hover:text-white hover:bg-green-900">
                <a href="{{ route('projects.create') }}">{{ __('New Project') }}</a>
            </x-jet-secondary-button>
        </div>
        @endrole

        @if(session('message'))
            <div class="mb-4 bg-green-200 p-2 mt-2">
                {{ session('message') }}
            </div>
        @endif
        
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-700">
                    <thead class="bg-gray-200">
                        <tr>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Title') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Assinged to') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Deadline') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Tasks') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Status') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Actions') }}
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach ($projects as $project)
                            <tr>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    <a href="{{ route('projects.show', $project) }}" class="no-underline">
                                        {{ $project->title }}</a>
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    {{ $project->user->name }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    {{ $project->client->name }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-500 border-b border-grey-light">
                                    {{ $project->deadline_at->format('d-m-Y') }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    {{ $project->completed_tasks }}/{{ $project->tasks_count }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    {{ $project->status }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    <div class="flex gap-2 justify-items-start">
                                        @can('access-projects', $project)
                                            <x-jet-secondary-button class="bg-yellow-400 text-gray-600 hover:text-black hover:bg-yellow-500">
                                                <a href="{{ route('projects.edit', $project) }}">{{ __('Edit') }}</a>
                                            </x-jet-secondary-button>
                                                                        
                                            <form method="POST" action="{{ route('projects.destroy', $project) }}">
                                                @csrf
                                                @method('DELETE')
                                                <x-jet-secondary-button type="submit" onclick="return confirm('Are you sure?')" class="bg-red-500 text-gray-800 hover:text-black hover:bg-red-600 inline-block">
                                                    {{ __('Delete') }}
                                                </x-jet-secondary-button>
                                            </form>                                      
                                        @endcan
                                    </div>
                                </td>
                            </tr>  
                        @endforeach 
                    </tbody>
                </table>
            </div>
            <div class="mt-2">
                {{ $projects->links() }}
            </div>
        </div>
    </div>
</x-app-layout>