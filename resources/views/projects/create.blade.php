<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Project') }}
        </h2>
    </x-slot>

    @role('admin')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6">

                    <form method="POST" action="{{ route('projects.store') }}">
                        @csrf
                        <div class="space-y-6">

                            {{-- Title --}}
                            <div>
                                <x-jet-label for="title" value="{{ __('Title') }}" />
                                <x-jet-input id="title" class="block w-full mt-1" type="text" name="title" :value="old('title')" autofocus autocomplete="title" />
                                <x-jet-input-error for="title" class="mt-2" />
                            </div>

                            {{-- Body --}}
                            <div>
                                <x-jet-label for="description" value="{{ __('Description') }}" />
                                <x-textarea name="description" id="description" rows="6" class="block w-full mt-1"></x-textarea>
                                <x-jet-input-error for="description" class="mt-2" />
                            </div>

                            {{-- Schedule --}}
                            <div>
                                <x-jet-label for="deadline_at" value="{{ __('Deadline') }}" />
                                <x-pikaday name="deadline_at" format="YYYY-MM-DD" />
                                <x-jet-input-error for="deadline_at" class="mt-2" />
                            </div>

                            {{-- Category --}}
                            <div>
                                <x-jet-label for="user_id" value="{{ __('Assigned user') }}" />
                                <select name="user_id" id="user_id" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                                    <option value="">Please select a user</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                <x-jet-input-error for="user_id" class="mt-2" />
                            </div>

                            {{-- Category --}}
                            <div>
                                <x-jet-label for="client_id" value="{{ __('Assigned client') }}" />
                                <select name="client_id" id="client_id" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                                    <option value="">Please select a client</option>
                                    @foreach ($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                    @endforeach
                                </select>
                                <x-jet-input-error for="client_id" class="mt-2" />
                            </div>

                            {{-- Category --}}
                            <div>
                                <x-jet-label for="status" value="{{ __('Status') }}" />
                                <select name="status" id="status" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                                    <option value="">Please select a status</option>
                                    @foreach (App\Models\Project::STATUS as $status)
                                        <option
                                            value="{{ $status }}" {{ old('status') == $status ? 'selected' : '' }}>{{ ucfirst($status) }}</option>
                                    @endforeach
                                </select>
                                <x-jet-input-error for="status" class="mt-2" />
                            </div>

                        </div>

                        <div class="flex justify-end mt-6">
                            <x-jet-button>
                                {{ __('Create') }}
                            </x-jet-button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    @endrole
</x-app-layout>