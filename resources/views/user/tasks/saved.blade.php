<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @forelse($tasks as $task)
            <div class="p-8 space-y-3 border-2 dark:border-blue-400 rounded-xl mb-4">
                <div class="flex items-center justify-between">
                    <div class="mt-2">
                        <span class="inline-block dark:text-blue-500">
                            <x-fluentui-task-list-square-rtl-24 class="h-12 w-12"/>
                        </span>
                    </div>
                    @if (!auth()->user()->savedTasks()->get()->contains($task->id))
                        <a href="#">
                            <x-uni-favorite-thin class="h-7 w-7 inline-block"/>
                        </a>
                    @else
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 text-yellow-400" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                        </a>
                    @endif
                </div>

                <h1 class="text-2xl font-semibold capitalize dark:text-gray-700">{{ $task->title }}</h1>
                
                <p class="dark:text-gray-500">
                    {{ $task->description }}
                </p>
                <div class="flex items-center justify-between">
                    @if(!$task->completed)
                        <div style="padding-top: 0.1em; padding-bottom: 0.1rem;" class="text-xs px-3 bg-orange-200 text-orange-800 rounded-full">Pending</div>
                    @else
                        <div style="padding-top: 0.1em; padding-bottom: 0.1rem" class="text-xs px-3 bg-teal-200 text-teal-800 rounded-full">Completed</div>
                    @endif
                </div>
            </div> 
            @empty
                You have no saved tasks.
            @endforelse
        </div>
    </div>

   
</x-app-layout>