<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tasks Assigned') }}
        </h2>
    </x-slot>

    @if(session('message'))
        <div class="mb-4 bg-green-200 p-2">
            {{ session('message') }}
        </div>
    @endif

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <livewire:user.user-dashboard >
        </div>
    </div>
</x-app-layout>