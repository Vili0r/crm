<div class="relative" x-data="{ isOpen: false }">
    <x-slot name="trigger">
    <!-- red dot -->
    @if(auth()->user()->unreadNotifications->count())
      <div class="absolute right-0 p-1 bg-red-400 rounded-full animate-ping"></div>
      <div class="absolute right-0 p-1 bg-red-400 border rounded-full"></div>
    @endif
    
    <button
      @click="isOpen = !isOpen"
      class="p-2 bg-gray-100 rounded-full hover:bg-gray-200 focus:outline-none focus:ring"
    >
      <svg
        class="w-6 h-6 text-gray-500"
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="2"
          d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
        />
      </svg>
    </button>

    <div
      @click.away="isOpen = false"
      x-show.transition.opacity="isOpen"
      class="
        absolute
        z-50
        w-48
        max-w-md
        mt-3
        transform
        bg-white
        rounded-md
        shadow-lg
        -translate-x-3/4
        min-w-max
      "
    >
    </x-slot>

    <x-slot name="content">
      <div class="p-4 font-medium border-b">
        <span class="text-gray-800">Notifications</span>
      </div>
      <ul class="flex flex-col p-2 my-2 space-y-1">
        <div>
          @forelse(auth()->user()->unreadNotifications as $notification)
            <form action="{{ route('notifications.markNotification', $notification) }}" method="POST">
                @csrf
                <li>
                  @hasrole('admin')
                    <button type="submit" class="block px-2 py-1 transition rounded-md hover:bg-gray-200">
                      [{{ $notification->created_at->diffForHumans() }}] User {{ $notification->data['name'] }} ({{ $notification->data['email'] }}) has just registered.
                    </button>
                  @else
                    <button type="submit" class="block px-2 py-1 transition rounded-md hover:bg-gray-200">
                      [{{ $notification->created_at->diffForHumans() }}] Project title: {{ $notification->data['title'] }} to be completed until {{ $notification->data['deadline'] }} has been assigned to you.
                    </button>
                  @endhasrole
                </li>
            </form>
          @empty
              There are no new notifications
          @endforelse
      </div>
      </ul>
    </div>
</x-slot>
</div>