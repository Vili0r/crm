<div class="py-6 px-4">
    <form wire:submit.prevent="update">

        <div class="space-y-6">
            {{-- Title --}}
            <div>
                <x-jet-label for="title" value="{{ __('Title') }}" />
                <x-jet-input id="title" class="block w-full mt-1" type="text" wire:model.defer="task.title" :value="old('title')" autofocus autocomplete="title" />
                <x-jet-input-error for="title" class="mt-2" />
            </div>

            {{-- Description --}}
            <div>
                <x-jet-label for="description" value="{{ __('Description') }}" />
                <textarea wire:model.defer="task.description" id="description" rows="6" class="block w-full mt-1">{{ $task->description }}</textarea>
                <x-jet-input-error for="description" class="mt-2" />
            </div>

            <div class="flex justify-end mt-6">
                <x-jet-button>
                    {{ __('Update') }}
                </x-jet-button>
            </div>
        </div>
    </form>
</div>