<div class="items-center hidden px-2 space-x-2 md:flex-1 md:flex md:mr-auto md:ml-5">
<!-- search icon --><svg
    class="w-5 h-5 text-gray-500"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
>
    <path
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-width="2"
    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
    />
</svg>

<input
    type="text"
    placeholder="Search"
    wire:model.debounce.300ms="search"
    class="
    px-4
    py-3
    rounded-md
    hover:bg-gray-100
    lg:max-w-sm
    md:py-2 md:flex-1
    focus:outline-none
    md:focus:bg-gray-100 md:focus:shadow md:focus:border
    "
/>

<div class="overflow-y-auto overflow-x-hidden absolute right-0 z-20 mt-10 w-full max-h-96 bg-white rounded shadow-lg">
    @foreach($results as $group => $entries)
        <div class="px-3 py-2 text-xs font-bold tracking-wider text-indigo-600 uppercase bg-indigo-200">
            {{ $group }}
        </div>

        <ul>
            @foreach($entries as $entry)
                <li class="flex items-center px-3 py-2 font-normal no-underline cursor-pointer hover:bg-indigo-100">
                    <a href="{{ $entry['linkTo'] }}">
                        @foreach($entry['fields'] as $name => $value)
                            <div class="text-sm text-blue-700">{{ $value }}</div>
                        @endforeach
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach

    @if (strlen($search) >= 3 && !count($results))
        <div class="flex items-center px-3 py-2 font-normal">No results found.</div>
    @endif
</div>
</div>

