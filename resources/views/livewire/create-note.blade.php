<div class="w-full py-6 px-4">
    @foreach ($notes as $note)
    <div class="flex">
        <div class="flex flex-col items-center mr-4">
          <div>
            <div class="flex items-center justify-center w-10 h-10 border rounded-full">
              <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-500" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 13l-5 5m0 0l-5-5m5 5V6" />
              </svg>
            </div>
          </div>
          <div class="w-px h-full bg-gray-300"></div>
        </div>
        <div class="pb-8 ">
          <p class="mb-2 text-xl font-bold text-gray-600">{{ $note->user->name }}</p>
          <span class="text-sm font-semibold text-gray-600">{{ $note->created_at->diffForHumans() }}</span>
          <h4 class="text-gray-700">
            {{ $note->body }}
          </h4>
        </div>
    </div>
    @endforeach
        
      
        {{-- <div class="flex">
          <div class="flex flex-col items-center mr-4">
            <div>
              <div class="flex items-center justify-center w-10 h-10 border rounded-full">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-500" fill="none" viewBox="0 0 24 24"
                  stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                </svg>
              </div>
            </div>
          </div>
          <div class="pt-1">
            <p class="mb-2 text-lg font-bold text-gray-600">Done</p>
          </div>
        </div>
      </div> --}}
      
    <form wire:submit.prevent="create" class="mt-3">
        <div class="space-y-6">
            {{-- Description --}}
            <div>
                <x-jet-label for="body" value="{{ __('Add a Note') }}" />
                <textarea wire:model.debounce.1000ms="body" id="body" rows="4" class="block w-full mt-1"></textarea>
                <x-jet-input-error for="body" class="mt-2" />
            </div>

            <div class="flex justify-end mt-6">
                <x-jet-button>
                    {{ __('Add') }}
                </x-jet-button>
            </div>
        </div>
    </form>
</div>