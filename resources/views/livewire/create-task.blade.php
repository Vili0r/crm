<div class="py-6 px-4">
    <form wire:submit.prevent="create">

        <div class="space-y-6">
            {{-- Title --}}
            <div>
                <x-jet-label for="title" value="{{ __('Title') }}" />
                <x-jet-input id="title" class="block w-full mt-1" type="text" wire:model.defer="title" autofocus autocomplete="title" />
                <x-jet-input-error for="title" class="mt-2" />
            </div>

            {{-- Description --}}
            <div>
                <x-jet-label for="description" value="{{ __('Description') }}" />
                <textarea wire:model.defer="description" id="description" rows="6" class="block w-full mt-1"></textarea>
                <x-jet-input-error for="description" class="mt-2" />
            </div>

            <div class="flex justify-end mt-6">
                <x-jet-button>
                    {{ __('Create') }}
                </x-jet-button>
            </div>
        </div>
    </form>
</div>