<div>
    @foreach ($projects as $project)
        <div class="max-w-5xl px-8 py-4 mx-auto mb-4 bg-white rounded-lg shadow-md " 
            x-data="{ 
                open:false,
            }">
            <div class="flex items-center justify-between">
                <span class="text-sm font-light text-gray-600">
                    <x-phosphor-calendar-check-duotone class="h-7 w-7 inline-block"/>
                    {{ $project->deadline_at->format('d-m-Y') }}
                </span>
                <progress value="{{ $project->completed_tasks }}" max="{{ $project->tasks_count }}" class="w-1/2"> </progress>
                <span >{{ $project->completed_tasks }}/{{ $project->tasks_count }}</span>
                <button onclick='Livewire.emit("openModal", "create-note", {{ json_encode([$project]) }})' class="px-3 py-1 text-sm font-bold text-gray-100 transition-colors duration-200 transform bg-gray-600 rounded cursor-pointer hover:bg-gray-500">
                    <x-uni-comment-add-o class="h-7 w-7 inline-block pr-1"/>
                    Add notes
                </button>
            </div>
            
            <div class="flex items-center justify-between">
                <div class="mt-2">
                    <a href="#" @click="open = !open" class="text-2xl font-bold text-gray-700 hover:text-gray-600 hover:underline">{{ $project->title }}</a>
                    <p class="mt-2 text-gray-600 ">{{ Str::words($project->description, 50) }}</p>
                </div>
            </div>
            
            <div x-show="open" class="flex items-center justify-start mt-4 gap-2">              
                @forelse($project->tasks as $task)   
                <div class="p-8 space-y-3 border-2 dark:border-blue-400 rounded-xl">
                    <div class="flex items-center justify-between">
                        <div class="mt-2">
                            <span class="inline-block dark:text-blue-500">
                                <x-fluentui-task-list-square-rtl-24 class="h-12 w-12"/>
                            </span>
                        </div>
                        @if (auth()->user()->savedTasks()->get()->contains($task->id))
                            <a href="#" wire:click.prevent="unsaveTask({{ $task->id }})">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 text-yellow-400" viewBox="0 0 20 20" fill="currentColor">
                                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                </svg>
                            </a>
                        @else
                            <a href="#" wire:click.prevent="saveTask({{ $task->id }})">
                                <x-uni-favorite-thin class="h-7 w-7 inline-block"/>
                            </a>
                        @endif
                    </div>

                    <h1 class="text-2xl font-semibold capitalize dark:text-gray-700">{{ $task->title }}</h1>
                    
                    <p class="dark:text-gray-500">
                        {{ $task->description }}
                    </p>
                    <input  wire:change="toggleTask({{ $task->id }})" 
                        {{ $task->completed ? 'checked' : '' }} type="checkbox" class="rounded-full" />
                </div> 
                @empty
                    There are no tasks.
                @endforelse 
            </div>
        </div>
    @endforeach
</div>
