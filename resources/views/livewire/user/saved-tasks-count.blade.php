<div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
    <x-jet-nav-link href="{{ route('user.saved_tasks') }}" :active="request()->routeIs('user.saved_tasks')">
        {{ __('Saved Tasks') }} ({{ $savedAmount }})
    </x-jet-nav-link>
</div>
