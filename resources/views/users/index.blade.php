<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Management') }}
        </h2>
    </x-slot>
    @can('manage users')
        {{-- Users table --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <table class="text-left w-full border-collapse"> <!--Border collapse doesn't work on this site yet but it's available in newer tailwind versions -->
                    <thead>
                    <tr>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">#</th>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Name</th>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Email</th>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Nr of Projects</th>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Role</th>
                        <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $key=>$user)
                            <tr class="hover:bg-grey-lighter">
                                <td class="py-4 px-6 border-b border-grey-light">{{ $key+1 }}</td>
                                <td class="py-4 px-6 border-b border-grey-light">{{ $user->name }}</td>
                                <td class="py-4 px-6 border-b border-grey-light">{{ $user->email}}</td>
                                <td class="py-4 px-6 border-b border-grey-light">{{ $user->projects_count }}</td>
                                @foreach($user->roles()->pluck('name') as $role)
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $role }}</td>
                                @endforeach                               
                                <td class="py-4 px-6 border-b border-grey-light justify-center">   
                                    <div class="flex gap-2">
                                        <x-jet-secondary-button class="bg-yellow-400 text-gray-600 hover:text-black hover:bg-yellow-500">
                                            <a href="{{ route('admin.users.edit', $user) }}">{{ __('Edit') }}</a>
                                        </x-jet-secondary-button>
                                        
                                        <form method="POST" action="{{ route('admin.users.destroy', $user) }}">
                                            @csrf
                                            @method('DELETE')
                                            <x-jet-secondary-button type="submit" onclick="return confirm('Are you sure?')" class="bg-red-500 text-gray-800 hover:text-black hover:bg-red-600 inline-block">
                                                {{ __('Delete') }}
                                            </x-jet-secondary-button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            There are no users
                        @endforelse                          
                    </tbody>
                </table>  
            </div>
            <div class="mt-2">
                {{ $users->links() }}
            </div>
        </div>
    </div>
    @endcan
</x-app-layout>