<x-app-layout>
  
    <div class="py-3">
        @role('admin')
        <div class="mb-3 ml-8">
            <x-jet-secondary-button class="bg-green-700 text-gray-300 hover:text-white hover:bg-green-900">
                <a href="{{ route('clients.create') }}">{{ __('Add Client') }}</a>
            </x-jet-secondary-button>
        </div>
        @endrole

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-700">
                    <thead class="bg-gray-200">
                        <tr>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                            {{ __('Company') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                            {{ __('Vat') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                            {{ __('Address') }}
                            </th>
                            <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider border-b border-grey-light">
                                {{ __('Actions') }}
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach ($clients as $client)
                            <tr>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-900 border-b border-grey-light">
                                    {{ $client->name }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-normal text-gray-500 border-b border-grey-light">
                                    {{ $client->vat_number }}
                                </td>
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    {{ $client->full_address }}
                                </td>
                                @role('admin')
                                <td class="p-4 whitespace-nowrap text-sm font-semibold text-gray-900 border-b border-grey-light">
                                    <div class="flex gap-2 justify-items-start">
                                        <x-jet-secondary-button class="bg-yellow-400 text-gray-600 hover:text-black hover:bg-yellow-500">
                                            <a href="{{ route('clients.edit', $client) }}">{{ __('Edit') }}</a>
                                        </x-jet-secondary-button>

                                        <form method="POST" action="{{ route('clients.destroy', $client) }}">
                                            @csrf
                                            @method('DELETE')
                                            <x-jet-secondary-button type="submit" onclick="return confirm('Are you sure?')" class="bg-red-500 text-gray-800 hover:text-black hover:bg-red-600 inline-block">
                                                {{ __('Delete') }}
                                            </x-jet-secondary-button>
                                        </form>
                                    </div>
                                </td>
                                @endrole
                            </tr>  
                        @endforeach 
                    </tbody>
                </table>
            </div>
            <div class="mt-2">
                {{ $clients->links() }}
            </div>
        </div>
    </div>
</x-app-layout>