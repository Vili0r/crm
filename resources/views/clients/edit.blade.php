<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Client') }}
        </h2>
    </x-slot>
    
    @role('admin')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                
                <div class="p-6">
                    <form method="POST" action="{{ route('clients.update', $client) }}">
                        @csrf
                        @method('PUT')

                        <div class="space-y-6">
                            <!-- Company Name -->
                            <div class="mb-3">
                                <x-jet-label for="name" value="{{ __('Company Name') }}" />
                                <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" :value="$client->name" autofocus autocomplete="name" />
                                <x-jet-input-error for="name" class="mt-2" />
                            </div>
                
                            <!-- Vat -->
                            <div class="mb-3">
                                <x-jet-label for="vat_number" value="{{ __('Vat Number') }}" />
                                <x-jet-input id="vat_number" class="block w-full mt-1" type="text" name="vat_number" :value="$client->vat_number" autofocus autocomplete="vat_number" />
                                <x-jet-input-error for="vat_number" class="mt-2" />
                            </div>

                            <!-- Address -->
                            <div class="mb-3">
                                <x-jet-label for="address" value="{{ __('Address') }}" />
                                <x-jet-input id="address" class="block w-full mt-1" type="text" name="address" :value="$client->address" autofocus autocomplete="address" />
                                <x-jet-input-error for="address" class="mt-2" />
                            </div>
                            
                             <!-- Countru code -->
                             <div class="mb-3">
                                <x-jet-label for="country_code" value="{{ __('Country Code') }}" />
                                <x-jet-input id="country_code" class="block w-full mt-1" type="text" name="country_code" :value="$client->country_code" autofocus autocomplete="country_code" />
                                <x-jet-input-error for="country_code" class="mt-2" />
                            </div>

                            <!-- Zip Code -->
                            <div class="mb-3">
                                <x-jet-label for="zip_code" value="{{ __('Zip Code') }}" />
                                <x-jet-input id="zip_code" class="block w-full mt-1" type="text" name="zip_code" :value="$client->zip_code" autofocus autocomplete="zip_code" />
                                <x-jet-input-error for="zip_code" class="mt-2" />
                            </div>
                
                            <div class="flex justify-end mt-6">
                                <x-jet-button>
                                    {{ __('Update') }}
                                </x-jet-button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endrole
</x-app-layout>