<x-app-layout>

    @if(session('message'))
        <div class="mb-4 bg-green-200 p-2">
            {{ session('message') }}
        </div>
    @endif

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @hasrole('admin')
              <div class="grid grid-cols-1 gap-6 mt-6 md:grid-cols-2 lg:grid-cols-4">
                    <!-- Users chart card -->
                  <a href="{{ route('admin.users.index') }}" class="p-4 transition-shadow border rounded-lg shadow-sm hover:shadow-lg hover:bg-white">
                    <div class="flex items-start">
                        <div class="flex flex-col flex-shrink-0 space-y-2">
                          <span class="text-gray-400">Users</span>
                          <span class="text-lg font-semibold">{{ $users }}</span>
                        </div>
                        <div class="relative min-w-0 ml-auto h-14">
                          <canvas id="usersChart"></canvas>
                        </div>
                      </div>
                                       
                      <div>
                        <span class="inline-block px-2 text-sm text-white bg-green-300 rounded">xx%</span>
                        <span>from 1st of month</span>
                      </div>
                  </a>
            
                    <!-- Sessions chart card -->
                  <a href="{{ route('clients.index') }}" class="p-4 transition-shadow border rounded-lg shadow-sm hover:shadow-lg hover:bg-white">
                      <div class="flex items-start">
                        <div class="flex flex-col flex-shrink-0 space-y-2">
                          <span class="text-gray-400">Clients</span>
                          <span class="text-lg font-semibold">{{ $clients }}</span>
                        </div>
                        <div class="relative min-w-0 ml-auto h-14">
                          <canvas id="sessionsChart"></canvas>
                        </div>
                      </div>
                      <div>
                        <span class="inline-block px-2 text-sm text-white bg-green-300 rounded">xx%</span>
                        <span>from 1st of month</span>
                      </div>
                  </a>
            
                    <!-- Vists chart card -->
                  <a href="{{ route('projects.index') }}" class="p-4 transition-shadow border rounded-lg shadow-sm hover:shadow-lg hover:bg-white">
                      <div class="flex items-start">
                        <div class="flex flex-col flex-shrink-0 space-y-2">
                          <span class="text-gray-400">Projects</span>
                          <span class="text-lg font-semibold">{{ $projects }}</span>
                        </div>
                        <div class="relative min-w-0 ml-auto h-14">
                          <canvas id="vistsChart"></canvas>
                        </div>
                      </div>
                      <div>
                        <span class="inline-block px-2 text-sm text-white bg-green-300 rounded">xx%</span>
                        <span>from 1st of month</span>
                      </div>
                  </a>
            
                    <!-- Articles chart card -->
                  <a href="{{ route('tasks.index') }}" class="p-4 transition-shadow border rounded-lg shadow-sm hover:shadow-lg hover:bg-white">
                      <div class="flex items-start">
                        <div class="flex flex-col flex-shrink-0 space-y-2">
                          <span class="text-gray-400">Tasks</span>
                          <span class="text-lg font-semibold">{{ $tasks }}</span>
                        </div>
                        <div class="relative min-w-0 ml-auto h-14">
                          <canvas id="articlesChart"></canvas>
                        </div>
                      </div>
                      <div>
                        <span class="inline-block px-2 text-sm text-white bg-green-300 rounded">xx%</span>
                        <span>from 1st of month</span>
                      </div>
                  </a>
              </div>
           @endhasrole
        </div>
    </div>
</x-app-layout>
