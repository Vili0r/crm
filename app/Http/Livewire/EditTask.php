<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Illuminate\Support\Facades\Gate;
use LivewireUI\Modal\ModalComponent;

class EditTask extends ModalComponent
{
    public $task;

    public function mount(Task $task)
    {
        $this->task = $task;
    }

    public function render()
    {
        return view('livewire.edit-task');
    }

    public function update()
    {
        Gate::authorize('manage-tasks', $this->task);

        $this->validate();

        $this->task->save();

        $this->closeModal();

        return redirect()->route('tasks.index');
    }

    protected function rules(): array
    {
        return [
            'task.title' => ['required', 'string', 'min:5', 'max:25'],
            'task.description' => ['required', 'string', 'min:10', 'max:50'],
        ];
    }
}
