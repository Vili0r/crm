<?php

namespace App\Http\Livewire;

use App\Models\Note;
use App\Models\Project;
use LivewireUI\Modal\ModalComponent;

class CreateNote extends ModalComponent
{
    public Project $project;
    public $body;

    public function mount(Project $project)
    {
        $this->project = $project;
    }

    protected $rules =
        [
            'body' => ['required'],
        ];

    public function render()
    {
        $notes = Note::with('user')
            ->whereHas('projects', function($query){
            $query->where('id', $this->project->id);
        })->get();

        return view('livewire.create-note', compact('notes'));
    }

    public function create()
    {
        $this->validate();

        $this->project->notes()->create([
            'body' => $this->body,
            'user_id' => auth()->id(),
        ]);

        $this->closeModal();
    }
}
