<?php

namespace App\Http\Livewire;

use App\Models\Project;
use Illuminate\Support\Facades\Gate;
use LivewireUI\Modal\ModalComponent;

class CreateTask extends ModalComponent
{
    public Project $project;
    public $title;
    public $description;

    public function mount(Project $project)
    {
        $this->project = $project;
        $project->load('user');
    }

    protected $rules =
        [
            'title' => ['required', 'string', 'min:5', 'max:25'],
            'description' => ['required', 'string', 'min:10', 'max:50'],
        ];

    public function render()
    {
        return view('livewire.create-task');
    }

    public function create()
    {
        Gate::authorize('access-projects', $this->project);

        $this->validate();

        $this->project->tasks()->create([
            'title' => $this->title,
            'description' => $this->description,
        ]);

        $this->closeModal();

        return redirect()->route('projects.show', [
            'project' => $this->project
        ]);
    }
}
