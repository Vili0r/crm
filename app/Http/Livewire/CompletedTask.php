<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Livewire\Component;

class CompletedTask extends Component
{
    public $task;

    public function render()
    {
        return view('livewire.completed-task');
    }

    public function toggleTask($taskId)
    {
        $task = Task::find($taskId);

        $task->completed = !$task->completed;

        $task->save();

        return redirect()->route('projects.show', [
            'project' => $this->task->project->id
        ]);
    }
}
