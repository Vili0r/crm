<?php

namespace App\Http\Livewire\User;

use App\Models\Project;
use App\Models\Task;
use Livewire\Component;

class UserDashboard extends Component
{
    public $taskId;

    protected $listeners = ['taskCompleted' => 'render'];

    public function toggleTask($taskId)
    {
        $task = Task::find($taskId);

        $task->completed = !$task->completed;

        $task->save();

        $this->emit('taskCompleted');
    }

    public function saveTask($taskId)
    {
        auth()->user()->savedTasks()->attach($taskId);

        $this->emit('taskSaved');
    }

    public function unsaveTask($taskId)
    {
        auth()->user()->savedTasks()->detach($taskId);

        $this->emit('taskSaved');
    }

    public function render()
    {
        $projects = Project::withCount('tasks')
            ->where('user_id', auth()->id())
            ->get();

        return view('livewire.user.user-dashboard', compact('projects'));
    }
}
