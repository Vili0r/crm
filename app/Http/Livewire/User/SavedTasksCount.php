<?php

namespace App\Http\Livewire\User;

use App\Models\Task;
use Livewire\Component;

class SavedTasksCount extends Component
{
    protected $listeners = ['taskSaved' => 'render'];
    
    public function render()
    {
        $savedAmount = auth()->user()->savedTasks()->count();

        return view('livewire.user.saved-tasks-count', compact('savedAmount'));
    }
}
