<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectsRequest;
use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use App\Notifications\ProjectAssignedNotification;
use Illuminate\Support\Facades\Gate;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with(['user', 'client'])
            ->withCount('tasks')
            ->paginate(20);

        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('manage users');
        $users = User::all();
        $projects = Project::all();
        $clients = Client::all();

        return view('projects.create', compact('users', 'projects', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProjectsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectsRequest $request)
    {
        $this->authorize('manage users');
        $project = Project::create($request->validated());

        $user = User::find($request->user_id);

        $user->notify(new ProjectAssignedNotification($project));

        //Mail::to($user)->send(new AssignedProject($project));

        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        Gate::authorize('access-projects', $project);

        $project->load('tasks');
        
        return view('projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        Gate::authorize('access-projects', $project);

        $users = User::all();
        $clients = Client::all();

        return view('projects.edit', compact('project', 'users', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectsRequest  $request
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProjectsRequest $request, Project $project)
    {
        Gate::authorize('access-projects', $project);

        $project->update($request->validated());

        return redirect()->route('projects.index')->banner('Project updated successfully.');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        Gate::authorize('access-projects', $project);

        $project->delete();

        return redirect()->route('projects.index')->dangerBanner('Project deleted successfully.');
    }
}
