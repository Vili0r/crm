<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $this->authorize('manage users');

        $users = User::query()
            ->whereBetween('created_at', [now()->firstOfMonth(), now()])
            ->count();

        $clients = Client::query()
            ->whereBetween('created_at', [now()->firstOfMonth(), now()])
            ->count();

        $projects = Project::query()
            ->whereBetween('created_at', [now()->firstOfMonth(), now()])
            ->count();
            
        $tasks = Task::query()
            ->whereBetween('created_at', [now()->firstOfMonth(), now()])
            ->count();

        $notifications = auth()->user()->unreadNotifications;
        
        return view('admin.dashboard.index', compact('notifications', 'users', 'clients', 'projects', 'tasks'));
    }
}
