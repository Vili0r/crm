<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class AdminCalendarController extends Controller
{
    public function index()
    {
        return view('admin.calendars.index');
    }

    public function fetch()
    {
        $events = Project::selectRaw('title as title, deadline_at as start')
            ->get();

        return response()->json($events);
    }
}
