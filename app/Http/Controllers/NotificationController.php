<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function markNotification($notificationId)
    {
        auth()->user()
            ->unreadNotifications
            ->where('id', $notificationId)
            ->markAsRead();
        
        if (auth()->user()->hasRole('admin')) {
            return redirect()->route('admin.dashboard')->with('message', 'Notification was read.');
        } else {
            return redirect()->route('user.dashboard')->with('message', 'Notification was read.');
        }
        
    }
}
