<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {
        return view('user.calendars.index');
    }

    public function fetch()
    {
        $events = Project::selectRaw('title as title, deadline_at as start')
            ->where('user_id', auth()->id())
            ->get();

        return response()->json($events);
    }
}
