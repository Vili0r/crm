<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class SavedTaskController extends Controller
{
    public function index()
    {
        $tasks = Task::whereHas('savedUsers', function($query){
            $query->where('id', auth()->id());
        })->get();

        return view('user.tasks.saved', compact('tasks'));
    }
}
