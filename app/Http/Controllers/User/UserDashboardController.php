<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Project;

class UserDashboardController extends Controller
{
    public function index()
    {        
        return view('user.dashboard.index');
    }
}