<?php

namespace App\Providers;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-projects', function(User $user, Project $project) {
            return $user->can('manage users') || $project->user->id == auth()->id() || $user->hasRole('client');
        });

        Gate::define('manage-tasks', function(User $user, Task $task) {
            return $task->project->user->id == auth()->id() || $user->hasRole('client');
        });
    }
}
