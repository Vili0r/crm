<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'deadline_at',
        'user_id',
        'client_id',
        'status',
    ];

    public const STATUS = ['open', 'in progress', 'blocked', 'cancelled', 'completed'];

    protected $casts = [
        'deadline_at' => 'datetime:m-d-Y',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function getCompletedTasksAttribute()
    {
        return $this->tasks()->where('completed', 1)->count();
    }

    public function getAllTasksAttribute()
    {
        return $this->tasks()->count();
    }

    public function notes()
    {
        return $this->belongsToMany(Note::class);
    }
}
