<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'vat_number',
        'address',
        'country_code',
        'zip_code',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function getFullAddressAttribute()
    {
        return $this->address . ' , ' .$this->country_code . '  ' .$this->zip_code;
    }
}
