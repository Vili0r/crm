<?php

use App\Http\Controllers\Admin\AdminCalendarController;
use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\User\CalendarController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\User\SavedTaskController;
use App\Http\Controllers\User\UserDashboardController;
use App\Http\Livewire\CompletedTask;
use App\Http\Livewire\User\SavedTasks;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => 'auth'], function(){

    Route::get('redirects', [HomeController::class, 'index']);

    Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function(){
        Route::resource('users', UserController::class);
        Route::get('dashboard', [AdminDashboardController::class, 'index'])->name('dashboard');
        Route::get('calendars', [AdminCalendarController::class,  'index'])->name('calendars');
        Route::get('calendarsEvents', [AdminCalendarController::class,  'fetch']);
    });

    Route::group(['middleware' => ['role:user'], 'prefix' => 'user', 'as' => 'user.'], function(){
        Route::get('dashboard', [UserDashboardController::class, 'index'])->name('dashboard');
        Route::get('completed_task', CompletedTask::class);
        Route::get('saved_tasks', [SavedTaskController::class, 'index'])->name('saved_tasks');
        Route::get('calendars', [CalendarController::class,  'index'])->name('calendars');
        Route::get('calendarsEvents', [CalendarController::class,  'fetch']);
    });

    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
        Route::post('/{notification}', [NotificationController::class, 'markNotification'])->name('markNotification');
    });

    Route::resources([
        'clients' => ClientController::class,
        'projects' => ProjectController::class,
        'tasks' => TaskController::class,
    ]);

});
