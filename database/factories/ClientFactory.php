<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Clients>
 */
class ClientFactory extends Factory
{
    protected $model = Client::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(10),
            'vat_number' => rand(1000, 100000),
            'address' => $this->faker->text(30),
            'country_code' => $this->faker->text(5),
            'zip_code' => rand(1000, 10000),
        ];
    }
}
